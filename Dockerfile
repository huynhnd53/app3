# Use a lightweight web server image
FROM nginx:alpine

# Copy the HTML and JavaScript files into the web server's root directory
COPY . /usr/share/nginx/html